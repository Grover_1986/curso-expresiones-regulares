

En el texto siguiente:
555658
56-58-11
56.58.11
56.78-98
65 09 87
76y87r98

Definir un patrón que haga match a todas las líneas excepto a la la última, la que tiene letras.
Es decir, seleccionar todas sin importar el caracter de separación, excepto cuando los números están separados entre sí por letras.

Mi solución:
\d{2,2}[^a-z]?\d{2,2}[^a-z]?\d{2,2}

Solución mejorada:
(\d{2}\W?){3}