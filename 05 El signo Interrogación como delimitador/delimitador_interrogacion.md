¿Que pasa si queremos encontrar diferentes matches en una línea?
Lo que se haría es escoger un carácter ya sea dígito o palabra y despues q termine con un símbolo, como por ejm: la coma (,)

OJO:
     El símbolo de ? tiene dos usos. 
    1) Por una parte es que haya o no haya un carácter o el elemento de alguna clase.
    2) Y después nos sirve para q el match q encuentre de la expresión anterior sea lo más greedy, lo más pequeño, lo más concreto posible.
    
Ejemplos:
     .+?, 
     .+?[,\n]{1,1}


1
12
123
1234
12345
123-456-789-101-112
123.456.789.101
123 123 123
12345a678910
csv1,csv2,csv3,csv4,csv5
12345678910123
123, ,789,1012,2533
1234 53243
hola tambien
palabra
url: https://www.instagram.com/p/BXB4zsUlW5Z/