
Principio (^) y final de linea ($)
Estos dos caracteres indican en qué posición de la línea debe hacerse la búsqueda:

El ^ se utiliza para indicar el principio de línea
El $ se utiliza para indicar final de línea

^ ------------- $

Ejemplos:

          ^[^\d].*$    -> esto es para encontrar todos los match por línea q no empiecen con números
          
          ^\w+,\w+,\w+$     -> esto encuentra por linea un macth completo de palabras q terminan en (,) 

1
12
123
1234
12345
123456
12345678910
12345678910aaaa
csv1,csv2,csv3,csv4,csv5
csv1,csv2,csv3,csv4
csv1,csv2,csv3
12345678910123
123,2345,789,1012,2533
123453243
hola tambien
palabra
url: https://www.instagram.com/p/BXB4zsUlW5Z/