\w caracteres de palabras
\d - dígitos 
\s - espacios/invisibles en blanco
[0-9] ~ \d
[0-9a-zA-Z_] ~ /w
* greedy - todo
+ u-no o más
? cero o uno

Ahora encontraremos dos tipos básicos de conteo, que existan o que no existan, y ahi empiezan a aparecer carácteres dentro de nuestra construccion de clases de una manera ya no tan intuitiva.

- El 1ro es el * ,esto agarra 0 o muchos, y con .* se encuentra todos los carácteres, asi haya 0 o ninguno, es decir encuentra todos los carácteres
  Ahora ponemos \d* y esto quiere decir q a partir de un digito, busque todos

- El 2do es el + ,esto nos ayuda a encontrar 1 o más carácteres
  Si ponemos \d+ nos encuentra lo mismo q \d

- El 3ro es el ? ,esto chapa o 0 o 1, al darle \d? encuentra donde haya 0 digitos o 1 digito

Ejemplos:

1) Esto: \d+[a-z] , encuentra todos los digitos que terminen en una letra
2) Esto: \d+\w , es lo mismo q el anterior
3) Esto: \d*[a-z] , encuentra una letra final q tenga o no tenga números al inicio
4) Esto: \d*[a-z][a-z] , encuentra dos letras al final, tengan o no tengan números al inicio
4) Esto: \d*[a-z][a-z]? , quiere decir q haya despues7 de máximo dos letras, 0 números o 1 número
5) Esto: \d*[a-z]?s , me va a encontrar los digitos, asi haya o no haya q contengan 1 letra o no la contengan y terminen en la letra s

1
12
123
1234
12345
123456
12345a678910
12345678910aaaa
123453243
hola tambien
palabra
url: https://www.instagram.com/p/BXB4zsUlW5Z/

csv1,csv2,csv3,csv4,csv5
Rodrigo Jiménez Gutiérrez
5556581111
56-58-11-12
302-345-9876
esto.es.un.mail+gmail@mail.com
dominio.com
[LOG ENTRY] [ERROR] The system is unstable
[LOG ENTRY] [WARN] The system may be down
[LOG ENTRY] [LOG] Everything is OK
[LOG ENTRY] [LOG] [user:@beco] Logged in
[LOG ENTRY] [LOG] [user:@beco] Clicked here
[LOG ENTRY] [LOG] [user:@beco] Rated the app
[LOG ENTRY] [LOG] [user:@beco] Logged out