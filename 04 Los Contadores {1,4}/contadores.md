

Los contadores se escriben entre {1,4}, el 1er valor es el minimo y el 2do es el máximo  

Ejemplos:

     - \d{2,5} , con esto decimos q nos busque al menos los valores de 2 dígitos y los valores máximo de 5 dígitos
     - \d{4,} , aqui nos selecciona los valores que tienen como máximo 4 dígitos hasta el infinito
     - \w{4,} , aqui nos selecciona las palabaras q tienen 4 carácteres hasta el infinito
     - \d{3}[\-\. ]+ , cuando encontremos un signo entre nros o un espacio, podemos hacerle un Scape con corchetes y slash invertido [\-\. ]
1
12
123
1234
12345
123-456-789-101-112
123.456.789.101
123 123 123
12345a678910
12345678910123
1234 53243
hola tambien
palabra
url: https://www.instagram.com/p/BXB4zsUlW5Z/

csv1,csv2,csv3,csv4,csv5
Rodrigo Jiménez Gutiérrez
5556581111
56-58-11-12
302-345-9876
esto.es.un.mail+gmail@mail.com
dominio.com
[LOG ENTRY] [ERROR] The system is unstable
[LOG ENTRY] [WARN] The system may be down
[LOG ENTRY] [LOG] Everything is OK
[LOG ENTRY] [LOG] [user:@beco] Logged in
[LOG ENTRY] [LOG] [user:@beco] Clicked here
[LOG ENTRY] [LOG] [user:@beco] Rated the app
[LOG ENTRY] [LOG] [user:@beco] Logged out