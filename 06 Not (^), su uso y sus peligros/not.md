
Este caracter ^ nos permite negar una clase o construir “anticlases”, vamos a llamarlo así, que es: toda la serie de caracteres que no queremos que entren en nuestro resultado de búsqueda.

Para esto definimos una clase, por ejemplo: [ 0-9 ], y la negamos [ ^0-9 ] para buscar todos los caracteres que coincidan con cualquier caracter que no sea 0,1,2,3,4,5,6,7,8 ó 9

Ejm:
     [^0-5a-c]

1
12
123
1234
12345
123-456-789-101-112
123.456.789.101
123 123 123
12345a678910
csv1,csv2,csv3,csv4,csv5
12345678910123
123, ,789,1012,2533
1234 53243
hola tambien
palabra
url: https://www.instagram.com/p/BXB4zsUlW5Z/